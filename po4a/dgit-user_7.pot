# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-03-01 16:59+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: =head1
#: ../dgit.1:3 ../dgit.7:2 ../dgit-user.7.pod:1 ../dgit-nmu-simple.7.pod:1
#: ../dgit-maint-native.7.pod:1 ../dgit-maint-merge.7.pod:1
#: ../dgit-maint-gbp.7.pod:1 ../dgit-maint-debrebase.7.pod:1
#: ../dgit-downstream-dsc.7.pod:1 ../dgit-sponsorship.7.pod:1
#: ../git-debrebase.1.pod:1 ../git-debrebase.5.pod:1
#, no-wrap
msgid "NAME"
msgstr ""

#. type: =head1
#: ../dgit.1:1470 ../dgit.7:23 ../dgit-user.7.pod:447
#: ../dgit-nmu-simple.7.pod:137 ../dgit-maint-native.7.pod:126
#: ../dgit-maint-merge.7.pod:491 ../dgit-maint-gbp.7.pod:136
#: ../dgit-maint-debrebase.7.pod:747 ../dgit-downstream-dsc.7.pod:352
#: ../dgit-sponsorship.7.pod:321 ../git-debrebase.1.pod:619
#: ../git-debrebase.5.pod:678
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: =head1
#: ../dgit.7:4 ../dgit-user.7.pod:27 ../dgit-nmu-simple.7.pod:35
#, no-wrap
msgid "SUMMARY"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:3
msgid "dgit-user - making and sharing changes to Debian packages, with git"
msgstr ""

#. type: =head1
#: ../dgit-user.7.pod:5 ../dgit-maint-native.7.pod:5
#: ../dgit-maint-merge.7.pod:5 ../dgit-maint-gbp.7.pod:5
#: ../dgit-maint-debrebase.7.pod:5 ../dgit-downstream-dsc.7.pod:5
#: ../git-debrebase.1.pod:10 ../git-debrebase.5.pod:5
msgid "INTRODUCTION"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:7
msgid ""
"dgit lets you fetch the source code to every package on your system as if "
"your distro used git to maintain all of it."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:11
msgid ""
"You can then edit it, build updated binary packages (.debs)  and install and "
"run them.  You can also share your work with others."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:16
msgid ""
"This tutorial gives some recipes and hints for this.  It assumes you have "
"basic familiarity with git.  It does not assume any initial familiarity with "
"Debian's packaging processes."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:21
msgid ""
"If you are a package maintainer within Debian; a DM or DD; and/or a sponsee: "
"this tutorial is not for you.  Try L<dgit-nmu-simple(7)>, L<dgit-maint-"
"*(7)>, or L<dgit(1)> and L<dgit(7)>."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:29
msgid "(These runes will be discussed later.)"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:33
#, no-wrap
msgid ""
"    % dgit clone glibc jessie,-security\n"
"    % cd glibc\n"
"    % curl 'https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=28250;mbox=yes;msg=89' | patch -p1 -u\n"
"    % git commit -a -m 'Fix libc lost output bug'\n"
"    % gbp dch -S --since=dgit/dgit/sid --ignore-branch --commit\n"
"    % mk-build-deps --root-cmd=sudo --install\n"
"    % dpkg-buildpackage -uc -b\n"
"    % sudo dpkg -i ../libc6_*.deb\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:44
msgid "Occasionally:"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:48 ../dgit-user.7.pod:242
#, no-wrap
msgid ""
"    % git clean -xdf\n"
"    % git reset --hard\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:53
msgid "Later:"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:57
#, no-wrap
msgid ""
"    % cd glibc\n"
"    % dgit pull jessie,-security\n"
"    % gbp dch -S --since=dgit/dgit/sid --ignore-branch --commit\n"
"    % dpkg-buildpackage -uc -b\n"
"    % sudo dpkg -i ../libc6_*.deb\n"
"\n"
msgstr ""

#. type: =head1
#: ../dgit-user.7.pod:65
msgid "FINDING THE RIGHT SOURCE CODE - DGIT CLONE"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:69
#, no-wrap
msgid ""
"    % dgit clone glibc jessie,-security\n"
"    % cd glibc\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:74
msgid ""
"dgit clone needs to be told the source package name (which might be "
"different to the binary package name, which was the name you passed to \"apt-"
"get install\")  and the codename or alias of the Debian release (this is "
"called the \"suite\")."
msgstr ""

#. type: =head2
#: ../dgit-user.7.pod:80
msgid "Finding the source package name"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:82
msgid ""
"For many packages, the source package name is obvious.  Otherwise, if you "
"know a file that's in the package, you can look it up with dpkg:"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:88
#, no-wrap
msgid ""
"    % dpkg -S /lib/i386-linux-gnu/libc.so.6 \n"
"    libc6:i386: /lib/i386-linux-gnu/libc.so.6\n"
"    % dpkg -s libc6:i386\n"
"    Package: libc6\n"
"    Status: install ok installed\n"
"    ...\n"
"    Source: glibc\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:98
msgid ""
"(In this example, libc6 is a \"multi-arch: allowed\" package, which means "
"that it exists in several different builds for different architectures.  "
"That's where C<:i386> comes from.)"
msgstr ""

#. type: =head2
#: ../dgit-user.7.pod:104
msgid "Finding the Debian release (the \"suite\")"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:106
msgid ""
"Internally, Debian (and derived) distros normally refer to their releases by "
"codenames.  Debian also has aliases which refer to the current stable "
"release etc.  So for example, at the time of writing Debian C<jessie> "
"(Debian 8) is Debian C<stable>; and the current version of Ubuntu is "
"C<yakkety> (Yakkety Yak, 16.10).  You can specify either the codename "
"C<jessie> or the alias C<stable>.  If you don't say, you get C<sid>, which "
"is Debian C<unstable> - the main work-in progress branch."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:117
msgid "If you don't know what you're running, try this:"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:121
#, no-wrap
msgid ""
"    % grep '^deb' /etc/apt/sources.list\n"
"    deb http://the.earth.li/debian/ jessie main non-free contrib\n"
"    ...\n"
"    %\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:128
msgid ""
"For Debian, you should add C<,-security> to the end of the suite name, "
"unless you're on unstable or testing.  Hence, in our example C<jessie> "
"becomes C<jessie,-security>.  (Yes, with a comma.)"
msgstr ""

#. type: =head1
#: ../dgit-user.7.pod:135
msgid "WHAT DGIT CLONE PRODUCES"
msgstr ""

#. type: =head2
#: ../dgit-user.7.pod:137
msgid "What branches are there"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:139
msgid ""
"dgit clone will give you a new working tree, and arrange for you to be on a "
"branch named like C<dgit/jessie,-security> (yes, with a comma in the branch "
"name)."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:143
msgid ""
"For each release (like C<jessie>)  there is a tracking branch for the "
"contents of the archive, called C<remotes/dgit/dgit/jessie> (and similarly "
"for other suites).  This can be updated with C<dgit fetch jessie>.  This, "
"the I<remote suite branch>, is synthesized by your local copy of dgit.  It "
"is fast forwarding."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:152
msgid ""
"Debian separates out the security updates, into C<*-security>.  Telling dgit "
"C<jessie,-security> means that it should include any updates available in "
"C<jessie-security>.  The comma notation is a request to dgit to track "
"jessie, or jessie-security if there is an update for the package there."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:158
msgid ""
"(You can also dgit fetch in a tree that wasn't made by dgit clone.  If "
"there's no C<debian/changelog> you'll have to supply a C<-p>I<package> "
"option to dgit fetch.)"
msgstr ""

#. type: =head2
#: ../dgit-user.7.pod:162
msgid "What kind of source tree do you get"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:164
msgid ""
"If the Debian package is based on some upstream release, the code layout "
"should be like the upstream version.  You should find C<git grep> helpful to "
"find where to edit."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:168
msgid ""
"The package's Debian metadata and the scripts for building binary packages "
"are under C<debian/>.  C<debian/control>, C<debian/changelog> and C<debian/"
"rules> are the starting points.  The Debian Policy Manual has most of the in-"
"depth technical details."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:175
msgid ""
"For many Debian packages, there will also be some things in C<debian/patches/"
">.  It is best to ignore these.  Insofar as they are relevant the changes "
"there will have been applied to the actual files, probably by means of "
"actual comments in the git history.  The contents of debian/patches are "
"ignored when building binaries from dgitish git branches."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:185
msgid ""
"(For Debian afficionados: the git trees that come out of dgit are \"patches-"
"applied packaging branches without a .pc directory\".)"
msgstr ""

#. type: =head2
#: ../dgit-user.7.pod:190
msgid "What kind of history you get"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:192
msgid ""
"If you're lucky, the history will be a version of, or based on, the Debian "
"maintainer's own git history, or upstream's git history."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:197
msgid ""
"But for many packages the real git history does not exist, or has not been "
"published in a dgitish form.  So you may find that the history is a rather "
"short history invented by dgit."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:203
msgid ""
"dgit histories often contain automatically-generated commits, including "
"commits which make no changes but just serve to make a rebasing branch fast-"
"forward.  This is particularly true of combining branches like C<jessie,-"
"security>."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:210
msgid ""
"If the package maintainer is using git then after dgit clone you may find "
"that there is a useful C<vcs-git> remote referring to the Debian package "
"maintainer's repository for the package.  You can see what's there with "
"C<git fetch vcs-git>.  But use what you find there with care: Debian "
"maintainers' git repositories often have contents which are very confusing "
"and idiosyncratic.  In particular, you may need to manually apply the "
"patches that are in debian/patches before you do anything else!"
msgstr ""

#. type: =head1
#: ../dgit-user.7.pod:222 ../dgit-maint-gbp.7.pod:56
msgid "BUILDING"
msgstr ""

#. type: =head2
#: ../dgit-user.7.pod:224
msgid "Always commit before building"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:228
#, no-wrap
msgid ""
"    % wget 'https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=28250;mbox=yes;msg=89' | patch -p1 -u\n"
"    % git commit -a -m 'Fix libc lost output bug'\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:233
msgid ""
"Debian package builds are often quite messy: they may modify files which are "
"also committed to git, or leave outputs and temporary files not covered by "
"C<.gitignore>."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:237
msgid "If you always commit, you can use"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:247
msgid ""
"to tidy up after a build.  (If you forgot to commit, don't use those "
"commands; instead, you may find that you can use C<git add -p> to help "
"commit what you actually wanted to keep.)"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:252
msgid ""
"These are destructive commands which delete all new files (so you B<must> "
"remember to say C<git add>)  and throw away edits to every file (so you "
"B<must> remember to commit)."
msgstr ""

#. type: =head2
#: ../dgit-user.7.pod:257
msgid "Update the changelog (at least once) before building"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:261
#, no-wrap
msgid ""
"    % gbp dch -S --since=dgit/dgit/sid --ignore-branch --commit\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:265
msgid ""
"The binaries you build will have a version number which ultimately comes "
"from the C<debian/changelog>.  You want to be able to tell your binaries "
"apart from your distro's."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:270
msgid ""
"So you should update C<debian/changelog> to add a new stanza at the top, for "
"your build."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:274
msgid ""
"This rune provides an easy way to do this.  It adds a new changelog entry "
"with an uninformative message and a plausible version number (containing a "
"bit of your git commit id)."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:279
msgid ""
"If you want to be more sophisticated, the package C<dpkg-dev-el> has a good "
"Emacs mode for editing changelogs.  Alternatively, you could edit the "
"changelog with another text editor, or run C<dch> or C<gbp dch> with "
"different options.  Choosing a good version number is slightly tricky and a "
"complete treatment is beyond the scope of this tutorial."
msgstr ""

#. type: =head2
#: ../dgit-user.7.pod:287
msgid "Actually building"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:291
#, no-wrap
msgid ""
"    % mk-build-deps --root-cmd=sudo --install\n"
"    % dpkg-buildpackage -uc -b\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:296
msgid ""
"dpkg-buildpackage is the primary tool for building a Debian source package.  "
"C<-uc> means not to pgp-sign the results.  C<-b> means build all binary "
"packages, but not to build a source package."
msgstr ""

#. type: =head2
#: ../dgit-user.7.pod:302
msgid "Using sbuild"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:304
msgid ""
"You can build in an schroot chroot, with sbuild, instead of in your main "
"environment.  (sbuild is used by the Debian build daemons.)"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:309
#, no-wrap
msgid ""
"    % git clean -xdf\n"
"    % sbuild -c jessie -A --no-clean-source \\\n"
"             --dpkg-source-opts='-Zgzip -z1 --format=1.0 -sn'\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:315
msgid ""
"Note that this will seem to leave a \"source package\" (.dsc and .tar.gz)  "
"in the parent directory, but that source package should not be used.  It is "
"likely to be broken.  For more information see Debian bug #868527."
msgstr ""

#. type: =head1
#: ../dgit-user.7.pod:322
msgid "INSTALLING"
msgstr ""

#. type: =head2
#: ../dgit-user.7.pod:324
msgid "Debian Jessie or older"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:328
#, no-wrap
msgid ""
"    % sudo dpkg -i ../libc6_*.deb\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:332
msgid ""
"You can use C<dpkg -i> to install the .debs that came out of your package."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:335
msgid ""
"If the dependencies aren't installed, you will get an error, which can "
"usually be fixed with C<apt-get -f install>."
msgstr ""

#. type: =head2
#: ../dgit-user.7.pod:339
msgid "Debian Stretch or newer"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:343
#, no-wrap
msgid ""
"    % sudo apt install ../libc6_*.deb\n"
"\n"
msgstr ""

#. type: =head1
#: ../dgit-user.7.pod:347
msgid "Multiarch"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:349
msgid ""
"If you're working on a library package and your system has multiple "
"architectures enabled, you may see something like this:"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:355
#, no-wrap
msgid ""
"    dpkg: error processing package libpcre3-dev:amd64 (--configure):\n"
"     package libpcre3-dev:amd64 2:8.39-3~3.gbp8f25f5 cannot be configured because libpcre3-dev:i386 is at a different version (2:8.39-2)\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:360
msgid ""
"The multiarch system used by Debian requires each package which is present "
"for multiple architectures to be exactly the same across all the "
"architectures for which it is installed."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:364
msgid ""
"The proper solution is to build the package for all the architectures you "
"have enabled.  You'll need a chroot for each of the secondary "
"architectures.  This is somewhat tiresome, even though Debian has excellent "
"tools for managing chroots.  C<sbuild-debian-developer-setup> from the "
"package of the same name and C<sbuild-createchroot> from the C<sbuild> "
"package are good starting points."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:374
msgid ""
"Otherwise you could deinstall the packages of interest for those other "
"architectures with something like C<dpkg --remove libpcre3:i386>."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:378
msgid ""
"If neither of those are an option, your desperate last resort is to try "
"using the same version number as the official package for your own package.  "
"(The version is controlled by C<debian/changelog> - see above.)  This is not "
"ideal because it makes it hard to tell what is installed, and because it "
"will mislead and confuse apt."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:386
msgid "With the \"same number\" approach you may still get errors like"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:390
msgid ""
"trying to overwrite shared '/usr/include/pcreposix.h', which is different "
"from other instances of package libpcre3-dev"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:394
msgid ""
"but passing C<--force-overwrite> to dpkg will help - assuming you know what "
"you're doing."
msgstr ""

#. type: =head1
#: ../dgit-user.7.pod:397
msgid "SHARING YOUR WORK"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:399
msgid ""
"The C<dgit/jessie,-security> branch (or whatever) is a normal git branch.  "
"You can use C<git push> to publish it on any suitable git server."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:402
msgid ""
"Anyone who gets that git branch from you will be able to build binary "
"packages (.deb)  just as you did."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:406
msgid ""
"If you want to contribute your changes back to Debian, you should probably "
"send them as attachments to an email to the L<Debian Bug System|https://bugs."
"debian.org/> (either a followup to an existing bug, or a new bug).  Patches "
"in C<git-format-patch> format are usually very welcome."
msgstr ""

#. type: =head2
#: ../dgit-user.7.pod:413
msgid "Source packages"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:415
msgid ""
"The git branch is not sufficient to build a source package the way Debian "
"does.  Source packages are somewhat awkward to work with.  Indeed many "
"plausible git histories or git trees cannot be converted into a suitable "
"source package.  So I recommend you share your git branch instead."
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:423
msgid ""
"If a git branch is not enough, and you need to provide a source package but "
"don't care about its format/layout (for example because some software you "
"have consumes source packages, not git histories)  you can use this recipe "
"to generate a C<3.0 (native)> source package, which is just a tarball with "
"accompanying .dsc metadata file:"
msgstr ""

#. type: verbatim
#: ../dgit-user.7.pod:434
#, no-wrap
msgid ""
"    % echo '3.0 (native)' >debian/source/format\n"
"    % git commit -m 'switch to native source format' debian/source/format\n"
"    % dgit -wgf build-source\n"
"\n"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:440
msgid ""
"If you need to provide a good-looking source package, be prepared for a lot "
"more work.  You will need to read much more, perhaps starting with L<dgit-"
"nmu-simple(7)>, L<dgit-sponsorship(7)> or L<dgit-maint-*(7)>"
msgstr ""

#. type: textblock
#: ../dgit-user.7.pod:449 ../dgit-maint-native.7.pod:128
#: ../dgit-maint-merge.7.pod:493 ../dgit-maint-gbp.7.pod:138
msgid "dgit(1), dgit(7)"
msgstr ""
